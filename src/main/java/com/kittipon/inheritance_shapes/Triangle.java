/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.inheritance_shapes;

/**
 *
 * @author kitti
 */
public class Triangle extends Shape {

    private double b;
    private double h;

    public Triangle(double b, double h) {
        System.out.println("Triangle has created.");
        this.b = b;
        this.h = h;
    }

    @Override
    public double calArea() {
        double result = 0.5 * b * h;
        return result;
    }

    public void print() {
        System.out.println("Total area is " +calArea());
    }
}
