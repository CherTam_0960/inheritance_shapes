/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.inheritance_shapes;

/**
 *
 * @author kitti
 */
public class Circle extends Shape {

    private double r;
    public static final double pi = 22.0 / 7;

    public Circle(double r) {
        System.out.println("Circle has created.");
        this.r = r;
    }

    @Override
    public double calArea() {
        double result = pi * r * r;
        return pi * r * r;
    }
    
    public void print(){
        System.out.println("Total area is " +calArea());
    }
    
}
